# Web Development Fundamentals
- [X] [*Web Development Basics*](https://gitlab.com/rkcperea/ibm-wdb/-/tree/main/docs#web-development-basics) [2023-02-13]
- [X] [*Developing Sites for the Web*](#developing-sites-for-the-web) [2023-02-14]
  - [X] *start: 9:50pm; end: 11:12pm* (multitasking) [2023-02-14]
  - [X] *start: 2:10pm; end: 3:20pm* [2023-02-16]
  - [X] *start: 5:00pm; end: 5:50pm* [2023-02-17]
  - [X] *start: yesterday(mobile); end: 3:25pm* [2023-02-19] 
- [ ] Introduction to HTML and CSS
- [ ] Bringing Websites to Life with JavaScript
- [ ] Website Testing and Deployment
- [ ] Develop an Interactive Task List Web Page
- [ ] Your Future in Web Development: The Job Landscape

## Web Development Basics
- [X] *Course Overview* [2023-02-12]
- [X] [*How does a computer work?*](https://gitlab.com/rkcperea/ibm-wdb/-/tree/main/docs#how-does-a-computer-work) [2023-02-12]
- [X] [*Speaking code*](https://gitlab.com/rkcperea/ibm-wdb/-/tree/main/docs#speaking-code) [2023-02-12]
- [X] [*What is web development?*](https://gitlab.com/rkcperea/ibm-wdb/-/tree/main/docs#what-is-web-development) [2023-02-12]
- [X] [*Front-end development*](https://gitlab.com/rkcperea/ibm-wdb/-/tree/main/docs#front-end-development) [2023-02-12]
- [X] [*Back-end development*](https://gitlab.com/rkcperea/ibm-wdb/-/tree/main/docs#back-end-development) [2023-02-13]
- [X] [*A network of networks*](https://gitlab.com/rkcperea/ibm-wdb/-/tree/main/docs#a-network-of-networks) [2023-02-13]
- [X] [*Summary and final assessment*](https://gitlab.com/rkcperea/ibm-wdb/-/tree/main/docs#summary-and-final-assessment-1) [2023-02-13]

### How does a computer work?
there are 4 main functions:
- input, processing, output, and storage
  - short-term storage and long-term storage
    - [ ] next project: cookies management
- hardware and software
  - [ ] "The keyboard, microphone, and mouse are examples of I/O devices." next project: voice pitch checker.
  - Operating System, programs, etc.
- Computer and the internet.
  - web browsers and developers
  - wrappers (to do more research)
  - web apps vs web pages
  - storage, file system and graphics processors
- exam result 80%

[back on top](#web-development-fundamentals)

### Speaking code
- describe abstraction and its role in computer programming
  - computer languages
  - Reflect: Using the rice cooker needs the proper parameters: putting the correct rice is to water ratio in the container before pressing the cook button. We don't need to know how the rice cooker stops at the right time when the rice is cooked.
- explain different types of programming languages
  - machine, compiled and interpreted languages
  - [Grace Hopper](https://en.wikipedia.org/wiki/Grace_Hopper)
  - self-describing, interpreter, and runtime (active in memory).
  -
- describe how a computer reads code
  - on / off | [punchcards](https://www.ibm.com/ibm/history/ibm100/us/en/icons/punchcard/)
  - compiler, syntax
- explain the compilation process
  - syntax check, machine language, opens, read, and execute
- exam result 100%
[back on top](#web-development-fundamentals)

### What is web development?
- Describe web development
- Distinguish between the web developer and web designer roles
  - Web developer
    - layout, fonts and colors
    - interactivity
    - databases, servers, network configuration
    - application programming interfaces (APIs)
    - other server-side software that ensures a website performs correctly (like tests)
    - front-end and back-end code
    - works with web designer, architect, product manager, and others
    - based on design and functionality needs
    - Need to be an expert on front-end and/or back-end coding languages, frameworks, and libraries
  - Web designer
    - how elements of web page and website are presented to the user
    - easy to find information, pleasant colors, enough but not too much negative space
    - should support great experience for users with accessibility needs.
    - and more!
    - wireframes & prototypes based on user requirements
    - work with clients or users to translate vision into viable designs, and use these designs to create a site.
    - need familiarity with some front-end coding languages
- Explain why web development is important
  - 5 billion users
  - informational website, social media, e-commerce
- Describe the components of a website
  - web pages on the same domain name
     - The layout, content and media, style of elements, user input: links buttons + advanced methods, authentication
  - domain name + https:// + www. = uniform resource locator (URL)
    - navigation menu
  - browser
    - web host intercepts to tell the server:
      - the location of files
      - the services needed
    - user interface (UI) as interpreted
  - web server
    - network of computers that makes website viewable on the internet
- Identify the typical steps of the web development process
  - blueprint and workers: hire people, write tests
    - Planning and design
    - Coding 
      - iterative process "When developers can’t translate the design or a specific requirement into software code, developers might need to go back to the designer and project manager to update the design or requirements documentation."
    - Testing and publishing 
      - buttons and links
      - accessible: vision or hearing impairment
      - dev to be pushed: staging site 
      - to avoid breaking
  - [ ] requirements
  - [ ] wireframes
  - [ ] iterative process of coding
  - [ ] purchase and register a domain name
  - [ ] set up web hosting (pc or provider)
  - [ ] ready for testing
  - [ ] staging site, push to live. 
  - [ ] maintenance
  - website
    - web design
      - project plans, mockups and prototypes,system architecture diagrams
      - user stories, site map or navigation structure design, visual assets
    - web development
      - web server code, test plans
      - client code, client visuals
- Compare client-side and server-side web processes
  - 5 over 6. personalized content... 
- exam result 100%
[back on top](#web-development-fundamentals)

### Front-end development

- describe front-end development
  - user device, layout & visual design elements, how to input data
  - user experience and accessibility
- Identify areas where front-end development is needed today
  - user device, layout & visual design elements, how to input data
    - mobile ready, responsive web design, modern frameworks
    - search engine optimization (SEO), index of all websites: crawling the web,
      - rank: speed, mobile friendliness and accessibility, etc.
      - [SEO Starter Guide](https://developers.google.com/search/docs/fundamentals/seo-starter-guide): metadata matches content "adding appropriate information so search engines can properly crawl the site."
  - user experience and accessibility
    - description text for images, high-contrast/dark mode, responsive touch navigation.
    - [World Wide Web Consortium (W3C)](https://www.w3.org/) The W3C Web Accessibility Initiative (WAI)'s [Accessibility Standards](https://www.w3.org/WAI/standards-guidelines/)
  - to use in multiple devices
  - media renders properly 
- Identify challenges front-end developers face
  - Meeting technical demands.
  - Supporting compatibility.
  - Accommodating graceful degradation.
- Identify common languages for front-end development
  - 4/6 retaken to 6/6 HTML vs CSS vs JavaScript
- exam result 100%
[back on top](#web-development-fundamentals)

### Back-end development
- Describe back-end web development 
  - received, sorted, and operations tracked
  - servers, applications, and databases
  - working in the background to power a user’s front-end experience
  - server code: logic, integration, and (processes and stores) data management
  - priorities: security, data integrity, and server responsiveness
- Identify areas where back-end development is needed today
  - powerful tools for individuals and businesses
  - + dynamic content, API dev, ensure website and system security
  - dynamic vs static content
  - Application programming interface (API) for data and services, communication of apps
    - what type of request, and what data is returned in a secure transaction
  - Payments and personally identifiable information (PII)
  - database security and server-side encryption to ensure that their websites are compliant with PII and payment card industry (PCI) security standards.  
- Identify the common languages for back-end development 
  - Java - apps that require strong security
  - JavaScript - dynamic web pages
  - Python - communicate with database and process data
- exam result 100%
[back on top](#web-development-fundamentals)

### A network of networks
- Compare and contrast internet and World Wide Web
  - World Wide Network collection of all websites
  - internet, network of computers
    - 1960s packet switching data to a network, Advanced Research Projects Agency (ARPA)'s ARPANET
    - 1970s Transmission Control Protocol and the Internet Protocol (also known as TCP/IP) by Robert Kahn and Vinton Cerf. Distributed computer networks: NASA & US Dept of Energy
    - 1980s ARPANET used TCP/IP: network of networks. Tim Berners-Lee (TBL): World Wide Web, web, hypertext document, and browsers
    - 1990s TBL's info.cern.ch first web browser, server, and website. WWW was released, the first web server. CERN was open source. 1% public, others: private networks. 10M users of WWW. 10K servers.
    - Present: still growing and evolving: Smart devices, the Internet of Things, and robotics all use the internet, computer scientists are finding even more ways it can be used.
    - source: [Brief History of the Internet](https://www.internetsociety.org/internet/history-internet/brief-history-internet/)
- Describe how the internet works
  - Enter: Internet server provider (ISP) to modem and/or router to your network to computers.
  - Pass: URL in browser -> request -> ISP -> domain name server (DNS) -> numerical internet protocol (IP) address.
  - Settle: browser breaks request using TCP
  - Enjoy: message is in correct format and route.
- Identify the benefits of using cloud services in web development
  - company's website running on a server (on site?) no more.
  - server farms evolvedinto: Cloud computing or the cloud. 
    - Storing large amounts of data and rapidly reading the data from and writing it to disk drives.
    - Processing data. 
    - Providing computing power. 
    - Handling networking. 
    - Running specialized software. 
    - focus:
      - host websites: IBM, Amazon, and Google; security certificates, database storage, domain name hosting, artificial intelligence services, ...
      - Distribute (software) workloads: smaller websites, reducing download and load times. Phone <-> software on cloud, new feature? update software on servers in the cloud, (live update without user app update). **API call**.
      - Store and distribute data: **b**inary **l**arge **ob**ject (**blob**) storage; (In rails Active Storage) from small hobbyist sites to the largest enterprise sites. 
      - Scale based on needs, before: had to upgrade, buy another computer, or borrow one. Many people overbought. Businesses and individuals can pay for only the capacity and power they use. Pay when needed, scale back when not anymore. There are cloud services that automatically scale up when demand is high and scale down when it gets back to normal.
- exam result 80%
[back on top](#web-development-fundamentals)

### Summary and final assessment 1
Describe the basic functions of a computer

Differentiate between hardware and software

Describe the types of programming languages

Explain how a computer reads code

Differentiate between web design and web development

Identify website components

Explain the main steps in the web development process

Differentiate between client-side and server-side processes

Identify the focus areas and common languages for front-end development

Identify the focus areas and common languages for back-end development

Differentiate between the World Wide Web and the internet

Explain how the cloud benefits web development

1
No matter the size, all computers have four basic functions: input, processing, output, and storage. 

2
Binary code, also known as machine code, is a system that represents all data as either 0’s or 1’s (false or true). It is the only language that a computer can understand.

3
Computers need hardware (the physical components) and software (the instructions) to accomplish the four functions. 

4
The concept of abstraction makes it possible to give a computer instruction without understanding the sheer complexity of the machine by hiding unnecessary details. 

5
A compiler translates human readable code into machine code by checking the syntax and translating human-readable code into binary code.   

6
The web development process consists of planning and designing, coding, testing, and publishing, and maintenance 

7
Client-side programs run on the user’s device, while server-side processes run on website servers. 

8
A front-end developer creates everything you see on a website, such as text and buttons.

9
A front-end developer primarily uses HTML, CSS, and JavaScript to build websites.   

10
A back-end developer writes code to power the front-end experience, including data management, and server responsiveness. 

11
A back-end developer uses JavaScript, Python, and Java to develop programs.

12
The World Wide Web is comprised of all the websites that you use. The internet is a network of computers and servers that provide the infrastructure for you to access the web.

13
Web developers can use cloud services to help them to build scalable, robust websites.

exam result 87%
[back on top](#web-development-fundamentals)

# Developing Sites for the Web
- [X] [*Web browsers and markup*](#web-browsers-and-markup) [2023-02-14]
  - Identify the purpose of web browsers
  - Describe markup languages and the challenges they can overcome
- [X] [*HTML: The Language of the Web*](#html-the-language-of-the-web) [2023-02-16]
  - Explain the structure, functions, and evolution of HTML
- [X] [*HTML 5: The Evolution of Hypertext*](#html5-the-evolution-of-hypertext) [2023-02-17]
  - Identify improvements that HTML5 introduced
- [X] [*Function of CSS: Styling Web pages*](#function-of-css-styling-web-pages) [2023-02-18]
  - Explain the features and functions of CSS
- [X] [*Function of JavaScript: Interaction*](#function-of-javascript-interaction) [2023-02-18]
  - Explain the features and functions of JavaScript
  - Identify the ways JavaScript interacts with CSS and HTML
- [X] [*Software Development Lifecycle (SDLC)*](#software-development-lifecycle-sdlc) [2023-02-19]
  - Identify phases in the software development lifecycle (SDLC)
- [X] [*From Requirements to Launch*](#from-requirements-to-launch) [2023-02-19]
  - Differentiate between waterfall and agile approaches to development
  - Highlight the scrum framework
- [X] [*Summary and Final Assessment*](#summary-and-final-assessment-2) [2023-02-19]* [2023-02-19]

- back to [*[2/7] Developing Sites for the Web*](#developing-sites-for-the-web) | [back on top](#web-development-fundamentals)

### Web browsers and markup
- Describe what a web browser does
  - Reflect: why markup? Similar to communicating ideas, without a common rules and laws, understanding would be difficult, much more would be rendering or replicating the idea. With a markup language, there's a shared data: the markup language, thus sending a smaller data / set of instruction would be faster. And if there would be differences, the tradeoff is better performance. (work together)
- Identify challenges of transmitting content over the internet that markup helps address
  - embedding -> linking
  - document size -> text-based. Bandwidth is the amount of data the network can transmit in a given period.
  - openness -> open-code or open-source vs propriety
- Describe the relationship between web browsers and markup
  - The [Web Hypertext Application Technology Working Group (WHATWG)](https://whatwg.org/) creates and updates [HTML standards](https://html.spec.whatwg.org/).
- Identify factors web developers consider when working with different browsers
  - How is the content experienced?
  - How is data stored?
  - What capabilities are available?
- exam result 100%
- back to [*[2/7] Developing Sites for the Web*](#developing-sites-for-the-web) | [back on top](#web-development-fundamentals)

### HTML: The language of the web
- Describe key developments in the history of HTML
  - schema (or outline): 1960s Generalized Markup Language (GML) -> Standardized GML (SGML) -> HTML 1989
    - A schema tells browsers how to correctly display and organize content.
- Identify the structure of HTML documents
  - structure: html -> head (resources + metadata), body; Extensible Markup Language (XML): mainly for data
  - short quiz 1/1 :)
  - tag structures: instruction names: strong, a, i, em...
  - attributes: style, resources(href, source), events(onclick), metadata (type=button)
  - element (from opening to closing tag)
  - short quiz 8/9
- Explain the presentation function of HTML
  - HTML is a declarative language: does what, decides how but [based on WHATWG](#web-browsers-and-markup)
  - a browser has their own HTML parser.
  - how browsers format and create interactions (JavaScript? Nope, Presentation)
  - describes how to format the elements of a document, but not how to express or process those descriptions (CSS? Nope, declarative language)
  - software that examines text in a document and separates elements (HTML parser? yup, parser)

- Evolution of HTML, Presentation x Events x Data processing. in HTML 5 distinctions have blurred.
  - focus: presentation, user experience, document structure over other types of processing.
  - evolved to be more compatible with different browsers and more accessible for the visually impaired: e.g. i & b tags -> em tag (for compatibility and accessibility could be i or b depending on browser parser, hopefully, Narrator might read them with emphasis)
- exam result 100%
- back to [*[2/7] Developing Sites for the Web*](#developing-sites-for-the-web) | [back on top](#web-development-fundamentals)

### HTML5: The evolution of HyperText
- Identify improvements HTML5 introduced to web development and users
  - obsolete Flash: not mobile-friendly, security challenges, had to be downloaded per browser
  - structure improvements (semantic structure): from divs -> header, nav, section, article, aside, footer
  - semantic tags describe exactly what they are designed to contain
  - Pages are ready to use, and the overall experience is improved.
  - HTML tags focus on the structure and content and allow other programming languages like CSS to manage graphic and layout elements.
  - Separate networks are able to communicate through secure tunnels.
    - Data and connectivity: autosave, basic data handling vs frameworks (built on the foundation that HTML5 provides)
      - Meta’s React, Evan You’s Vue.js
    - Interactivity, graphics and videos can be added without 3rd party plugins, plus geolocation APIs
    - Web applications, browser-only web apps
- Recognize the need for browsers to support HTML5
  - HTML5 specification defines what HTML should support, but the implementation is up to browser manufacturers
  - It takes regular diligence to stay current, and you should consider this a part of your job as a web developer.
- exam result 50% -> 100%
- back to [*[2/7] Developing Sites for the Web*](#developing-sites-for-the-web) | [back on top](#web-development-fundamentals)

### Function of CSS: Styling web pages
- Describe the history of cascading style sheets (CSS)
  - emphasis on web page experience over looks. power of presentation
  - from none -> per browser -> standard CSS (a declarative language)
  - Netscape's Navigation and Microsoft's Internet Explorer adopted CSS
  - attribute, style= construction, inline style AND cascading style sheets
- Summarize what a stylesheet language is
- Explain cascading style sheets
- Describe the function of CSS styles

[See Snippet 1](https://gitlab.com/rkcperea/ibm-wdb/-/snippets/2501972)
- back to [*[2/7] Developing Sites for the Web*](#developing-sites-for-the-web) | [back on top](#web-development-fundamentals)

### Function of JavaScript: Interaction
[see Snippet 2](https://gitlab.com/rkcperea/ibm-wdb/-/snippets/2501974)
- back to [*[2/7] Developing Sites for the Web*](#developing-sites-for-the-web) | [back on top](#web-development-fundamentals)

### Software Development Lifecycle (SDLC)
[see Snippet 3](https://gitlab.com/rkcperea/ibm-wdb/-/snippets/2502319)

- back to [*[2/7] Developing Sites for the Web*](#developing-sites-for-the-web) | [back on top](#web-development-fundamentals)

### Summary and Final Assessment 2
[see Snippet 4](https://gitlab.com/rkcperea/ibm-wdb/-/snippets/2502323)
1
The browser interprets HTML markup and reports using human-readable styles and formats when rendering an HTML document. 

2
Markup is wrapping text with indicators that guide what should be done with that text. 

3
Markups are text-based and include links to resources, making them easy to interpret and display. 

4
A properly structured HTML document includes the HTML tag, the head section, and the body section.

5
HTML attributes are additional data a tag might need to provide the parser with enough information to create the formatting the element needs. 

6
A semantic tag clearly describes what it does to the browser and the web developer. 

7
Frameworks are tools that heavily use a scripting language like JavaScript to handle data. 

8
Developers created the cascading style sheets (CSS) language to add styling to elements of the same class or id without needing to style every element in an HTML document. 

9
A style in a stylesheet has three basic parts: A selector, a node name, and a declaration block. The declaration block has one or more styles that are made up of properties and values.

10
A primary feature of CSS is that child elements get the style of parent elements unless they’re overridden. 

11
CSS mainly focuses on website presentation, and the code can program the text, style, and function of the site.

12
JavaScript controls interactivity on a website, such as button clicking, popups, and refreshing a web page.

13
JavaScript, HTML, and CSS all work together through code to display a web page and all its features.

14
The waterfall approach is a step-by-step action plan that flows downward like a waterfall.  

15
The agile approach is iterative, meaning the process is repeated often and sometimes the same steps can be repeated several times. 

16
Scrum is an iterative approach to implementing agile in which teams deliver work in sprints and participate in scrum ceremonies. 

exam result 87%

- back to [*[2/7] Developing Sites for the Web*](#developing-sites-for-the-web) | [back on top](#web-development-fundamentals)

## Introduction to HTML and CSS
- [ ] Set up a web page with HTML
  - Describe how web developers use HTML elements to build a web page
  - Identify and explain common HTML elements
- [ ] Controlling Elements with Attributes
  - Define HTML attributes and uses
  - Identify and explain common attributes
  - Identify coding organization techniques that benefit web developers and users
  - Describe methods and techniques to apply CSS to HTML
  - Describe the CSS box model and the components of CSS boxes
- [ ] HTML5: Organizing Code
  - Describe best practices for writing HTML and CSS
  - Describe key features of an integrated development environment (IDE) that help improve workflow for web developers
- back to [*[3/7] Introduction to HTML and CSS*](#introduction-to-html-and-css) | [back on top](#web-development-fundamentals)

### Set up a web page with HTML

- back to [*[3/7] Introduction to HTML and CSS*](#introduction-to-html-and-css) | [back on top](#web-development-fundamentals)

### Controlling Elements with Attributes
- back to [*[3/7] Introduction to HTML and CSS*](#introduction-to-html-and-css) | [back on top](#web-development-fundamentals)

### HTML5: Organizing Code
- back to [*[3/7] Introduction to HTML and CSS*](#introduction-to-html-and-css) | [back on top](#web-development-fundamentals)

### Coding with style with CSS
- back to [*[3/7] Introduction to HTML and CSS*](#introduction-to-html-and-css) | [back on top](#web-development-fundamentals)

### CSS Box Model
- back to [*[3/7] Introduction to HTML and CSS*](#introduction-to-html-and-css) | [back on top](#web-development-fundamentals)

### Best practices for writing HTML and CSS
- back to [*[3/7] Introduction to HTML and CSS*](#introduction-to-html-and-css) | [back on top](#web-development-fundamentals)

### Summary and Final Assessment 3
- back to [*[3/7] Introduction to HTML and CSS*](#introduction-to-html-and-css) | [back on top](#web-development-fundamentals)